#include <iostream>
#include <complex>
#include <cmath>

using namespace std;
const complex<double> I(0.0,1.0);

template<typename T> void fft(T*,int, complex<double>, complex<double>*);
template<typename T> void fft(T*,int, complex<double>*);

int main(){
   int n = 4;
   double signal[] = {0,1.0,2.0,3.0};
   complex<double> X[n];
   fft(signal,n,X);
   for(int i = 0; i <n; i++) cout << X[i] << endl;
   return 0;
}


template<typename T>
void fft(T *signal, int n, complex<double> w, complex<double>* out){
   if(n==1){
      out[0] = signal[0];
      return;
   }
   double s1[n/2];
   double s2[n/2];
   for(int i = 0; i < n/2; i++){
      s1[i] = signal[2*i];
      s2[i] = signal[2*i+1];
   }
   complex<double> o1[n/2];
   fft(s1,n/2,pow(w,2),o1);
   complex<double> o2[n/2];
   fft(s2,n/2,pow(w,2),o2);
   for(int i = 0; i < n/2; i++){
      out[i] = o1[i] + pow(w,i)*o2[i];
      out[i+n/2] = o1[i] - pow(w,i)*o2[i];
   }
   return;
}

template<typename T>
void fft(T *signal, int n, complex<double>* out){
   complex<double> w = exp(-2*I*M_PI/n);
   fft(signal,n,w,out);
   return;
}
